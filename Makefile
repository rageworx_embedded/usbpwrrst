# Compiler preset.
CC_PREFIX = $(PREFIX)
CC_PATH =

# Compiler definitions
GCC = $(CC_PATH)$(CC_PREFIX)gcc
GPP = $(CC_PATH)$(CC_PREFIX)g++
AR  = $(CC_PATH)/ar

# Base PATH
BASE_PATH = .
LIB_PATH  = $(BASE_PATH)/lib
SRC_PATH  = $(BASE_PATH)/src

# TARGET settings
TARGET_PKG = usbpwrctl
TARGET_DIR = ./bin
TARGET_OBJ = ./obj

# Installation settings
INSTALL_PATH = /usr/local/bin

# DEFINITIONS
DEFS +=

# Compiler optiops 
COPTS  = -std=c++11
COPTS += -fexceptions -O2 -s
#COPTS += -g -DDEBUG
COPTS += -ffast-math -fopenmp

# CC FLAG
CFLAGS += -I$(SRC_PATH)
CFLAGS += $(DEFS)
CFLAGS += $(COPTS)

# LINK FLAG
LFLAGS += -Llib
LFLAGS += -lpthread

# Sources
SRCS     = $(wildcard $(SRC_PATH)/*.cpp)

# Make object targets from SRCS.
OBJS     = $(SRCS:$(SRC_PATH)/%.cpp=$(TARGET_OBJ)/%.o)

# Linking objects
ALLOBJS  = $(TARGET_OBJ)/*.o

.PHONY: prepare clean continue install uninstall

all: prepare continue

continue: $(TARGET_DIR)/$(TARGET_PKG)

prepare:
	@mkdir -p $(TARGET_DIR)
	@mkdir -p $(TARGET_OBJ)

clean:
	@echo "Cleaning built targets ..."
	@rm -rf $(TARGET_DIR)/$(TARGET_PKG).*
	@rm -rf $(TARGET_INC)/*.h
	@rm -rf $(TARGET_OBJ)/*.o

$(OBJS): $(TARGET_OBJ)/%.o: $(SRC_PATH)/%.cpp
	@echo "Compiling $< ..."
	@$(GPP) $(CFLAGS) -c $< -o $@

$(TARGET_DIR)/$(TARGET_PKG): $(OBJS) $(HOBJS)
	@echo "Generating(Linking) $@ ..."
	@$(GPP) $(ALLOBJS) $(CFLAGS) $(LFLAGS) -o $@
	@sync
	@echo "done."

install: $(TARGET_DIR)/$(TARGET_PKG)
	@echo "Installing $(TARGET_PKG) ..."
	@cp -f $< $(INSTALL_PATH)

uninstall: $(INSTALL_PATH)/$(TARGET_PKG)
	@ecoh "Uninstalling $< ..."
	@rm -f $<
