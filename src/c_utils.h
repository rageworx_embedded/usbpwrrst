#ifndef __C_UTILS_H__
#define __C_UTILS_H__

#include <vector>
#include <string>

namespace commutils
{
    int bytes2hexstr( const unsigned char* src, unsigned len, char* &out );
    int hexstr2bytes( const char* src, unsigned len, unsigned char* &out );

    std::vector<std::string> split( const std::string& str, const std::string& delimiter );
    std::string trim(const std::string &s);
    std::string uppercase( const std::string &s );

    const char* removeFileExtension( const char* file_name );
    const char* removeFilePath( const char* file_path );
    const char* stripFilePath( const char* refpath );
};

#endif // __C_UTILS_H__
