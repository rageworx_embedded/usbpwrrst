#include <sys/time.h>
#include <unistd.h>
#include <iostream>

#include "tick.h"

namespace
{
	class __GET_TICK_COUNT
	{
		public:
			__GET_TICK_COUNT()
			{
				if (gettimeofday(&tv_, NULL) != 0)
					throw 0;
			}

			timeval tv_;
	};

	__GET_TICK_COUNT timeStart;
}

unsigned long GetTickCount()
{
	return (unsigned long)GetTickCountF();
}

unsigned long GetTickCount2()
{
	timeval tv;
	gettimeofday(&tv, NULL);
	
	return (unsigned long)(tv.tv_sec * 1000 + tv.tv_usec / 1000);
}

float GetTickCountF()
{
	static time_t	secStart	= timeStart.tv_.tv_sec;
	static time_t	usecStart	= timeStart.tv_.tv_usec;
								
	timeval tv;
	gettimeofday(&tv, NULL);
	
	return (float)(tv.tv_sec - secStart) * 1000.f \
           + (float)(tv.tv_usec - usecStart) / 1000.f;
}
