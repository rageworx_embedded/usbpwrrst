#ifndef __GETTICK_H__
#define __GETTICK_H__

unsigned long GetTickCount();
unsigned long GetTickCount2();
float GetTickCountF();

#endif /// of __GETTICK_H__
