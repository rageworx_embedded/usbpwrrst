#include <unistd.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>

#include "c_utils.h"

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

int commutils::bytes2hexstr( const unsigned char* src, unsigned len, char* &out )
{
    if ( ( src != NULL ) && ( len > 0 ) )
    {
        unsigned actualen = len * 2;
        out = new char[ actualen ];
        if ( out != NULL )
        {
            for( unsigned cnt=0; cnt<len; cnt++ )
            {
                sprintf( &out[cnt*2], "%02X", src[cnt] );
            }

            return (int)actualen;
        }
    }

    return -1;
}

int commutils::hexstr2bytes( const char* src, unsigned len, unsigned char* &out )
{
    if ( ( src != NULL ) && ( len > 1 ) )
    {
        unsigned actualen = len / 2;
        out = new unsigned char[ actualen ];
        if ( out != NULL )
        {
            for( unsigned cnt=0; cnt<actualen; cnt++ )
            {
                char tmpstr[5] = {0};
                sprintf( tmpstr, "0x%c%c",
                         src[cnt*2],
                         src[cnt*2+1] );
                out[cnt] = (unsigned char)strtol( tmpstr, NULL, 16 );
            }

            return (int)actualen;
        }
    }

    return -1;
}

vector<string> commutils::split( const string& str, const string& delimiter )
{
    vector<string> strings;

    string::size_type pos = 0;
    string::size_type prev = 0;

    while ( (pos = str.find(delimiter, prev)) != string::npos )
    {
        strings.push_back(str.substr(prev, pos - prev));
        prev = pos + 1;
    }

    strings.push_back(str.substr(prev));

    return strings;
}

string commutils::trim(const string &s)
{
    auto  wsfront=find_if_not(s.begin(),s.end(),[](int c){return isspace(c);});
    return string( wsfront,
                   find_if_not( s.rbegin(),
                                string::const_reverse_iterator(wsfront),
                                [](int c)
                                {
                                    return isspace(c);
                                }
                              ).base()
                 );
}

string commutils::uppercase( const string &s )
{
    string ret(s.size(), char());
    for(unsigned int i = 0; i < s.size(); ++i)
        ret[i] = (s[i] <= 'z' && s[i] >= 'a') ? s[i]-('a'-'A') : s[i];
    return ret;
}

const char* commutils::removeFileExtension( const char* file_name )
{
    if ( file_name == NULL )
        return NULL;

    static string fnameext;

    fnameext = file_name;

    size_t last_path_div_pos = fnameext.find_last_of( '/' );

    if ( last_path_div_pos != string::npos )
    {
        fnameext = fnameext.substr( last_path_div_pos );
    }

    size_t fname_len = fnameext.size();
    size_t extpos = fnameext.find_last_of( '.' );

    if ( extpos != string::npos )
    {
        fnameext.erase( extpos, fname_len );
    }

    return fnameext.c_str();
}

const char* commutils::removeFilePath( const char* file_path )
{
    if ( file_path == NULL )
        return NULL;

    static string fname;

    fname = file_path;

    size_t last_path_div_pos = fname.find_last_of( '/' );

    if ( last_path_div_pos != string::npos )
    {
        fname = fname.substr( last_path_div_pos + 1 );
    }

    return fname.c_str();
}


const char* commutils::stripFilePath( const char* refpath )
{
    if ( refpath == NULL )
        return NULL;

    static string fpath;

    fpath = refpath;

    size_t last_path_div_pos = fpath.find_last_of( "/" );

    if ( last_path_div_pos != string::npos )
    {
        fpath = fpath.substr( 0, last_path_div_pos );
    }

    return fpath.c_str();
}
