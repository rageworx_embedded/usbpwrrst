#include <unistd.h>
#include <dirent.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <string>
#include <vector>
#include <algorithm>

#include "proctool.h"
#include "c_utils.h"
#include "version.h"

using namespace std;
using namespace proctool;
using namespace commutils;

#define DEFAULT_USB_PATH    "/sys/bus/usb/devices"

typedef struct _usbinfo
{
    uint16_t idx;
    uint16_t vid;
    uint16_t pid;
    uint16_t bcd;
    string   serial;
    string   product;
}usbinfo;

#define USBINFOLIST         vector<usbinfo>

static string opt_me;
static bool   opt_list = false;
static bool   opt_reset = false;
static int    opt_usbidx = -1;

bool readf( const char* fn, char* data, size_t datalen )
{
    FILE* fp = fopen( fn, "r" );
    if ( fp != NULL )
    {
        fgets( data, datalen, fp );
        fclose( fp );

        return true;
    }

    return false;
}

bool writef( const char* fn, const char* data )
{
    FILE* fp = fopen( fn, "w" );
    if ( fp != NULL )
    {
        int reti = fputs( data, fp );
        fclose( fp );

        if ( reti >= 0 )
            return true;
    }

    return false;
}

uint16_t str2uint16( const char* x )
{
    uint32_t tx = 0;
    sscanf( x, "%x", &tx );
    return (uint16_t)tx;
}

void listUSB( USBINFOLIST& usblist )
{
    char    cmdlinepath[100] = {0};
    char    nameofproc[300] = {0};
    char*   pcompare = NULL;
    struct\
    dirent* dire = NULL;
    DIR*    dirproc = NULL;

    dirproc = opendir( DEFAULT_USB_PATH );
    if ( dirproc == NULL )
    {
        return;
    }

    while( dire = readdir( dirproc ) )
    {

        if ( dire->d_name != NULL )
        {
            string tmpname = dire->d_name;

            if ( tmpname.find( "usb" ) == 0 )
            {
                usbinfo item;
                char data[64]  = {0};
                char smap[128] = {0};

                string idxstr = tmpname.substr( 3 );

                item.idx = atoi( idxstr.c_str() );

                // read VID
                snprintf( smap, 128, "%s/%s/idVendor", 
                          DEFAULT_USB_PATH, tmpname.c_str() );
                if( readf( smap, data, 64 ) == true )
                {
                    item.vid = str2uint16( data );
                }
                memset( data, 0, 64 );

                // read PID
                snprintf( smap, 128, "%s/%s/idProduct", 
                          DEFAULT_USB_PATH, tmpname.c_str() );
                if( readf( smap, data, 64 ) == true )
                {
                    item.pid = str2uint16( data );
                }
                memset( data, 0, 64 );

                // read BCD
                snprintf( smap, 128, "%s/%s/bcdDevice", 
                          DEFAULT_USB_PATH, tmpname.c_str() );
                if( readf( smap, data, 64 ) == true )
                {
                    item.bcd = str2uint16( data );
                }
                memset( data, 0, 64 );

                // read Serial
                snprintf( smap, 128, "%s/%s/serial", 
                          DEFAULT_USB_PATH, tmpname.c_str() );
                if( readf( smap, data, 64 ) == true )
                {

                    item.serial = trim(data);
                }
                memset( data, 0, 64 );

                // read Descriptor
                snprintf( smap, 128, "%s/%s/product", 
                          DEFAULT_USB_PATH, tmpname.c_str() );
                 if( readf( smap, data, 64 ) == true )
                {
                    item.product = trim(data);
                }
                memset( data, 0, 64 );

                // add an item to list ..
                usblist.push_back( item );
            }
        }
    }

    closedir( dirproc );
}

int doList()
{
    int retv = -1;

    USBINFOLIST ulist;
    listUSB( ulist );

    if ( ulist.size() == 0 )
    {
        printf( "no devices found.\n" );
        return retv;
    }

    for ( size_t cnt=0; cnt<ulist.size(); cnt++ )
    {
        printf( "#%zu[usb%u],%04X:%04X/%04X/%16s/%s\n",
                cnt+1, 
                ulist[cnt].idx,
                ulist[cnt].vid, 
                ulist[cnt].pid,
                ulist[cnt].bcd,
                ulist[cnt].serial.c_str(),
                ulist[cnt].product.c_str() );
    }

    retv = 0;

    return retv;
}

int doReset()
{
    int retv = -1;

    // test sudoer ?
    int s_euid = geteuid();

    if ( s_euid > 10 )
    {
        printf( "Error: root previlage required, try with sudo.\n" );
        return retv;
    }

    USBINFOLIST ulist;
    listUSB( ulist );

    if ( ulist.size() == 0 )
    {
        printf( "no devices found.\n" );
        return retv;
    }

    size_t idx = 0;

    if ( ( ulist.size() < opt_usbidx ) || ( opt_usbidx == 0 ) )
    {
        printf( "Error: USB index is out of boundary : %d\n", opt_usbidx );
        return retv;
    }

    idx = opt_usbidx - 1;

    printf( "target: usb%u,%04X:%04X/%04X/%s/%s\n",
            ulist[idx].idx,
            ulist[idx].vid, 
            ulist[idx].pid,
            ulist[idx].bcd,
            ulist[idx].serial.c_str(),
            ulist[idx].product.c_str() );

    char strprevwkup[32] = {0};
    char strprevpwr[32]  = {0};
    char smap_wkup[128]  = {0};
    char smap_pwr[128]   = {0};

    snprintf( smap_wkup, 128, 
              "%s/usb%u/power/wakeup",
              DEFAULT_USB_PATH, ulist[idx].idx );

    snprintf( smap_pwr, 128, 
              "%s/usb%u/power/level",
              DEFAULT_USB_PATH, ulist[idx].idx );

    printf( "backup previous setting ... " );
    
    if ( readf( smap_wkup, strprevwkup, 32 ) == false )
    {
        printf( "failure.\n" );
        printf( "Error: cannot read wakeup configuration.\n" );
    }

    if ( readf( smap_pwr, strprevpwr, 32 ) == false )
    {
        printf( "failure.\n" );
        printf( "Error: cannot read power configuration.\n" );
    }

    // strip off carrage return.
    size_t swkupl = strlen( strprevwkup );
    size_t spwrl  = strlen( strprevpwr );
    if ( swkupl > 0 )
    {
        if ( strprevwkup[ swkupl - 1 ] == '\n' )
        {
            strprevwkup[ swkupl - 1] = 0;
        }
    }

    if ( spwrl > 0 )
    {
        if ( strprevpwr[ spwrl - 1 ] == '\n' )
        {
            strprevpwr[ spwrl - 1 ] = 0;
        }
    }

    printf( "Ok.\n" );
    printf( " -- wakeup configuration : %s\n", strprevwkup );
    printf( " -- power configuration  : %s\n", strprevpwr );

    printf( "trying to reset usb ... " );
    fflush( stdout );

    bool need2wakeup_en = false;

    if ( strcmp( strprevwkup, "disabled" ) != 0 )
    {
        writef( smap_wkup, "disabled" );
        need2wakeup_en = true;
    }

    if ( writef( smap_pwr, "suspend" ) == false )
    {
        printf( "failure.\n" );
        printf( "Error: cannot write IO control.\n" );
        return retv;
    }

    // wait 1 sec.
    usleep( 1000000 );

    if ( writef( smap_pwr, strprevpwr ) == false )
    {
        printf( "failure.\n" );
        printf( "Error: cannot write IO control.\n" );
        return retv;
    }

    if ( need2wakeup_en == true )
    {
        if ( writef( smap_wkup, strprevwkup ) == false )
        {
            printf( "(warning, wakeup configure write failure)" );
        }
    }

    printf( "done.\n" );

    retv = 0;

    return retv;
}

int parseArgs( int argc, char** argv )
{
    int retv = 0;

    opt_me = argv[0];
    size_t mestp = opt_me.find_last_of( '/' );
    if ( mestp != string::npos )
    {
        opt_me = opt_me.substr( mestp + 1 );
    }

    if ( argc > 1 )
    {
        for( int cnt=1; cnt<argc; cnt ++ )
        {
            string tmpargv = argv[cnt];

            if ( ( tmpargv == "-l" ) || ( tmpargv == "--list" ) )
            {
                opt_list = true;
                retv++;
            }
            else
            if ( ( tmpargv == "-r" ) || ( tmpargv == "--reset" ) )
            {
                opt_reset = true;
                retv++;
            }
            else
            if ( ( opt_reset == true ) && ( opt_usbidx == -1 ) )
            {
                // test is numberic string ?
                if ( all_of( tmpargv.begin(), tmpargv.end(), ::isdigit ) == true )
                {
                    opt_usbidx = atoi( tmpargv.c_str() );

                    if ( opt_usbidx < -1 )
                    {
                        opt_usbidx = -1;
                    }
                }
            }
        }
    }

    return retv;
}

void showHelp()
{
    printf( "\n" );
    printf( "    usage: %s [option] (usb index)\n", opt_me.c_str() );
    printf( "\n" );
    printf( "    options:\n" );
    printf( "        -l, --list              : list up controllable USB.\n" );
    printf( "        -r, --reset (usb index) : reset USB in index bound.\n" );
    printf( "                                  -1 meaning all.\n" );
    printf( "\n" );
}

int main( int argc, char** argv )
{
    int retv   = 0;
    int vers[] = {VERSION_ARRAY};
    int argvs  = parseArgs( argc, argv );

    printf( "%s : USB power reset controller, v%d.%d.%d.%d | ", 
            opt_me.c_str(), vers[0], vers[1], vers[2], vers[3] );
    printf( "(C)2022 Raphael Kim\n" );

    if ( argvs == 0 )
    {
        showHelp();
        return 0;
    }

    if ( opt_list == true )
    {
        retv = doList();
    }

    if ( opt_reset == true )
    {
        if ( opt_usbidx >= 0 )
        {
            retv = doReset();
        }
        else
        {
            printf( "Error, not correct usb index.\n ");
        }
    }

    return retv;
}
