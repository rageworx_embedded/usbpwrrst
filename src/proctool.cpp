#include <unistd.h>
#include <dirent.h>

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cstdarg>
#include <cerrno>

#include <iostream>
#include <memory>
#include <array>
#include <string>

#include "tick.h"
#include "proctool.h"
#include "c_utils.h"

//////////////////////////////////////////////////////////////////////////

#define PROCDIR     "/proc/"

//////////////////////////////////////////////////////////////////////////

const char* blockedcmds[] = {
    "vi ",
    "vim ",
    "tail ",
    "nano ",
    "hexedit ",
    "gedit "
};
const unsigned blockedcmdslen = 6;

//////////////////////////////////////////////////////////////////////////

using namespace std;
using namespace commutils;

//////////////////////////////////////////////////////////////////////////

const unsigned exe_timed_out_ms = 5 * 1000;

//////////////////////////////////////////////////////////////////////////

bool isnumberic( const char* cst )
{
    for ( ; *cst; cst++ )
    {
        if ( ( *cst < '0' ) || ( *cst > '9' ) )
            return false;
    }

    return true;
}

//////////////////////////////////////////////////////////////////////////

pid_t proctool::findPIDbyname( const char* procname, bool exactlymatched, char* &fullname )
{
    char    cmdlinepath[100] = {0};
    char    nameofproc[300] = {0};
    char*   pcompare = NULL;
    pid_t   curproc = (pid_t) -1;
    struct\
    dirent* dire = NULL;
    DIR*    dirproc = NULL;

    dirproc = opendir( PROCDIR );
    if ( dirproc == NULL )
    {
        return curproc;
    }

    while( dire = readdir( dirproc ) )
    {
        if ( dire->d_name != NULL )
        {
            sprintf( cmdlinepath, "%s%s/cmdline", PROCDIR, dire->d_name );
        }

        FILE* cmdfd = fopen( cmdlinepath, "rt" );
        if ( cmdfd != NULL )
        {
            fscanf( cmdfd, "%s", nameofproc );
            fclose( cmdfd );
            char* ppos = strrchr( nameofproc, '/' );
            if ( ppos != NULL )
                pcompare = ppos + 1;
            else
                pcompare = nameofproc;

            if ( exactlymatched == true )
            {
                if ( strcmp( pcompare, procname ) == 0 )
                {
                    if ( fullname == NULL )
                    {
                        fullname = strdup( pcompare );
                    }
                    else
                    {
                        strcpy( fullname, pcompare );
                    }

                    curproc = (pid_t) atoi ( dire->d_name );
                    closedir( dirproc );
                    return curproc;
                }
            }
            else
            {
                if ( strstr( pcompare, procname ) != NULL )
                {
                    if ( fullname != NULL )
                    {
                        strcpy( fullname, pcompare );
                    }

                    curproc = (pid_t) atoi ( dire->d_name );
                    closedir( dirproc );
                    return curproc;
                }
            }
        }
    }

    closedir( dirproc );
    return curproc;
}
    
int proctool::checksafecmd( const char* cmd )
{
    string chk = trim( cmd );

    // check unsafe words -
    unsigned bc_len = blockedcmdslen;
    
    for( unsigned cnt=0; cnt<bc_len; cnt++ )
    {
        if ( chk.find( blockedcmds[cnt] ) != string::npos )
        {
            return -1;
        }
    }

    return 0;
}

const char* proctool::execute( const char* cmd )
{
    static string result;

    if ( cmd == NULL )
        return NULL;

    if ( result.size() > 0 )
    {
        result.clear();
    }

#ifdef DEBUG_PROCTOOL
    printf( "#DEBUG#execute( \"%s\" );\n", cmd );    
#endif

    array<char, 4096> buffer;
    unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);

    if ( pipe == NULL )
    {
        string ses = strerror( errno );
        
        if ( ses.size() > 0 )
        {
            result = strerror( errno );
        }

        return result.c_str();
    }

    unsigned prev_t = GetTickCount();

    while ( fgets(buffer.data(), buffer.size(), pipe.get()) != NULL )
    {
        unsigned next_t = GetTickCount();

        if ( buffer.size() > 0 )
        {
            result += buffer.data();
            
            //prevent to timed out.
            prev_t = next_t;
        }
        else
        if ( next_t - prev_t > exe_timed_out_ms )
        {
#ifdef DEBUG_PROCTOOL
            printf( "proc(%s)timed out\n", cmd );
#endif
            result += cmd;
            result += " timed out";

            string ses = strerror( errno );

            if ( ses.size() > 0 )
            {
                result += strerror( errno );
            }

            break;
        }
    }

#ifdef DEBUG_PROCTOOL
    printf( "#DEBUG#execute return : %s\n", result );
#endif

    return result.c_str();
}

