#ifndef __PROCTOOL_H__
#define __PROCTOOL_H__

#include <csignal>

namespace proctool
{
    pid_t findPIDbyname( const char* procname, bool exactlymatched, \
                         char* &fullname );
    int checksafecmd( const char* cmd );
    const char* execute( const char* cmd );
};

#endif /// of __PROCTOOL_H__
